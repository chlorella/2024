import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  site: process.env.DOMAIN,
  base: '/2024',
  outDir: 'public',
  publicDir: 'static',
});
